import XCTest
import Alamofire
@testable import gbshopeliseev

class BasketServiceTest: XCTestCase {

    var networkService: NetworkServiceMock!
    var basketService: BasketService!

    override func setUp() {
        super.setUp()

        networkService = NetworkServiceMock()
        basketService = BasketServiceImpl(baseUrl: URL(string: "http://failUrl")!, networkService: networkService)
    }

    // MARK: - TestAddToBasket
    // Тестирую запрос на добавление продукта в корзину

    func testSuccesAddToBasket() {
        networkService.result = CommonResult(result: 1, userMessage: "Удалось")

        var responseResult: CommonResult?
        basketService.addToBasket(idProduct: 1, quantity: 1) { (result) in
            responseResult = result
        }
        XCTAssertNotNil(responseResult)
    }

    func testFailureAddToBasket() {
        var responseResult: CommonResult?
        basketService.addToBasket(idProduct: 1, quantity: 1) { (result) in
            responseResult = result
        }
        XCTAssertNil(responseResult)
    }

    // MARK: - TestDeleteFromBasket
    // Тестирую запрос на удаление продукта из корзины

    func testSuccesDeleteFromBasket() {
        networkService.result = CommonResult(result: 1, userMessage: "Удалось")

        var responseResult: CommonResult?
        basketService.deleteFromBasket(idProduct: 1) { (result) in
            responseResult = result
        }
        XCTAssertNotNil(responseResult)
    }

    func testFailureDeleteFromBasket() {
        var responseResult: CommonResult?
        basketService.deleteFromBasket(idProduct: 1) { (result) in
            responseResult = result
        }
        XCTAssertNil(responseResult)
    }

    // MARK: - TestGetBasket
    // Тестирую запрос на получение продуктов пользователя

    func testSuccesGetBasket() {
        networkService.result = CommonResult(result: 1, userMessage: "Удалось")

        var responseResult: CommonResult?
        basketService.getBasket(idUser: 1) { (result) in
            responseResult = result
        }
        XCTAssertNotNil(responseResult)
    }

    func testFailureGetBasket() {
        var responseResult: CommonResult?
        basketService.getBasket(idUser: 1) { (result) in
            responseResult = result
        }
        XCTAssertNil(responseResult)
    }
}
