import XCTest
import Alamofire
@testable import gbshopeliseev

class AuthServiceTest: XCTestCase {

    var networkService: NetworkServiceMock!
    var authService: AuthService!

    override func setUp() {
        super.setUp()

        networkService = NetworkServiceMock()
        authService = AuthServiceImpl(baseUrl: URL(string: "http://failUrl")!, networkService: networkService)
    }

    // MARK: - TestLogin
    // Тестирую запрос на авторизацию пользователя

    func testSuccessLogin() {
        let user = User(userId: 1,
                        login: "login",
                        name: "name",
                        lastname: "lastname")
        networkService.result = LoginResult(result: 1, user: user)

        var resultUser: User?
        authService.login(userName: "111", password: "111") { (user) in
            resultUser = user
        }
        XCTAssertNotNil(resultUser)
    }

    func testFailureLogin() {
        var resultUser: User?
        authService.login(userName: "111", password: "111") { (user) in
            resultUser = user
        }
        XCTAssertNil(resultUser)
    }

    // MARK: - TestLogout
    // Тестирую запрос на выход пользователем из приложения

    func testSuccessLogout() {
        networkService.result = CommonResult(result: 1, userMessage: "Удалось")
        var responseResult: CommonResult?
        authService.logout(userId: 1) { (response) in
            responseResult = response
        }
        XCTAssertNotNil(responseResult)
    }

    func testFailureLogout() {
        var responseResult: CommonResult?
        authService.logout(userId: 1) { (response) in
            responseResult = response
        }
        XCTAssertNil(responseResult)
    }

    // MARK: - TestRegistration
    // Тестирую запрос на регистрацию пользователя

    func testSuccessRegistration() {
        networkService.result = CommonResult(result: 1, userMessage: "Удалось")

        var responseResult: CommonResult?

        authService.registerUser(user: UserFullInform(userId: "111",
                                                      login: "111",
                                                      password: "111",
                                                      email: "111",
                                                      gender: "111",
                                                      creditCard: "111",
                                                      bio: "111")) { (response) in
            responseResult = response
        }
        XCTAssertNotNil(responseResult)
    }

    func testFailureRegistration() {
        var responseResult: CommonResult?
        authService.registerUser(user: UserFullInform(userId: "111",
                                                      login: "111",
                                                      password: "111",
                                                      email: "111",
                                                      gender: "111",
                                                      creditCard: "111",
                                                      bio: "111")) { (response) in
            responseResult = response
        }
        XCTAssertNil(responseResult)
    }

    // MARK: - TestChangeUserData
    // Тестирую запрос на изменение данных пользователя

    func testSuccessChangeUserData() {
        networkService.result = CommonResult(result: 1, userMessage: "Удалось")

        var responseResult: CommonResult?
        authService.changeUserData(userId: 111,
                                   userName: "111",
                                   password: "111",
                                   email: "111",
                                   gender: "111",
                                   creditCard: "111",
                                   bio: "111") { (result) in
            responseResult = result
        }
        XCTAssertNotNil(responseResult)
    }

    func testFailureChangeUserData() {
        var responseResult: CommonResult?
        authService.changeUserData(userId: 111,
                                   userName: "111",
                                   password: "111",
                                   email: "111",
                                   gender: "111",
                                   creditCard: "111",
                                   bio: "111") { (result) in
            responseResult = result
        }
        XCTAssertNil(responseResult)
    }
}
