import XCTest
import Alamofire
@testable import gbshopeliseev

class NetworkServiceMock: NetworkService {

    var result: Any?

    func request<T: Decodable>(_ request: URLRequestConvertible,
                               completion: @escaping (T?) -> Void) {
        completion(result as? T)
    }
}
