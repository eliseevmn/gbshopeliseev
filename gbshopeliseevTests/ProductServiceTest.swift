import XCTest
import Alamofire
@testable import gbshopeliseev

class ProductServiceTest: XCTestCase {

    var networkService: NetworkServiceMock!
    var productService: ProductService!

    override func setUp() {
        super.setUp()

        networkService = NetworkServiceMock()
        productService = ProductServiceImpl(baseUrl: URL(string: "http://failUrl")!, networkService: networkService)
    }

    // MARK: - TestProductsList
    // Тестирую запрос на получение перечня продуктов

    func testSuccessProductsList() {
        let products = [ProductItem(productId: 1, productName: "111", productPrice: 111)]
        networkService.result = ProductsList(pageNumber: 1, products: products)

        var result: [ProductItem]?
        productService.getProductList(pageNumber: 1, categoryId: 1) { (response) in
            result = response
        }
        XCTAssertNotNil(result)
    }

    func testFailureProductsList() {
        var result: [ProductItem]?
        productService.getProductList(pageNumber: 1, categoryId: 1) { (response) in
            result = response
        }
        XCTAssertNil(result)
    }

    // MARK: - TestProductItem
    // Тестирую запрос на получение конкретного продукта

    func testSuccessProductsItem() {
        let product = ProductById(result: 1, productName: "111", productPrice: 111, productDesription: "111")
        networkService.result = product

        var result: ProductById?
        productService.getProductItem(productId: 1) { (response) in
            result = response
        }
        XCTAssertNotNil(result)
    }

    func testFailureProductsItem() {
        var result: ProductById?
         productService.getProductItem(productId: 1) { (response) in
             result = response
         }
        XCTAssertNil(result)
    }
}
