import XCTest
import Alamofire
@testable import gbshopeliseev

class ReviewServiceTest: XCTestCase {

    var networkService: NetworkServiceMock!
    var reviewService: ReviewService!

    override func setUp() {
        super.setUp()

        networkService = NetworkServiceMock()
        reviewService = ReviewServiceImpl(baseUrl: URL(string: "http://failUrl")!, networkService: networkService)
    }

    // Тестирую запрос на добавление отзыва

    func testSuccessAddReview() {
        networkService.result = CommonResult(result: 1, userMessage: "Удалось")

        var responseResult: CommonResult?
        reviewService.addReview(idUser: 1, text: "111") { (result) in
            responseResult = result
        }
        XCTAssertNotNil(responseResult)
    }

    func testFailureAddReview() {
        var responseResult: CommonResult?
        reviewService.addReview(idUser: 1, text: "111") { (result) in
            responseResult = result
        }
        XCTAssertNil(responseResult)
    }

    // Тестирую запрос на одобрение отзывы

    func testSuccessApproveReview() {
        networkService.result = CommonResult(result: 1, userMessage: "Удалось")

        var responseResult: CommonResult?
        reviewService.approveReview(idComment: 111) { (result) in
            responseResult = result
        }
        XCTAssertNotNil(responseResult)
    }

    func testFailureApproveReview() {
        var responseResult: CommonResult?
        reviewService.approveReview(idComment: 111) { (result) in
            responseResult = result
        }
        XCTAssertNil(responseResult)
    }

    // Тестирую запрос на удаление отзыва

    func testSuccessRemoveReview() {
        networkService.result = CommonResult(result: 1, userMessage: "Удалось")

        var responseResult: CommonResult?
        reviewService.removeReview(idComment: 111) { (result) in
            responseResult = result
        }
        XCTAssertNotNil(responseResult)
    }

    func testFailureRemoveReview() {
        var responseResult: CommonResult?
        reviewService.removeReview(idComment: 111) { (result) in
            responseResult = result
        }
        XCTAssertNil(responseResult)
    }
}
