import Foundation

/// Объект ответа с пользователем, получаемого при авторизации
struct LoginResult: Codable {
    let result: Int
    let user: User
}
