import Foundation

/// Описание количества объектов списка продуктов,
/// получаемых при сетевом запросе
struct ProductsList: Decodable {
    let pageNumber: Int
    let products: [ProductItem]

    enum CodingKeys: String, CodingKey {
        case pageNumber = "page_number"
        case products
    }
}

/// Описание обьекта продукта,
/// получаемого при сетевом запросе перечня товаров
struct ProductItem: Decodable, Hashable {
    let productId: Int
    let productName: String
    let productPrice: Int

    enum CodingKeys: String, CodingKey {
        case productId = "id_product"
        case productName = "product_name"
        case productPrice = "price"
    }

    func contains(filter: String?) -> Bool {
        guard let filter = filter else { return true }
        if filter.isEmpty { return true }
        let lowercasedFilter = filter.lowercased()
        return productName.lowercased().contains(lowercasedFilter)
    }
}

/// Объект продукта с полной информацией о нем
struct ProductById: Decodable, Hashable {
    let result: Int
    let productName: String
    let productPrice: Int
    let productDesription: String

    enum CodingKeys: String, CodingKey {
        case result
        case productName = "product_name"
        case productPrice = "product_price"
        case productDesription = "product_description"
    }
}
