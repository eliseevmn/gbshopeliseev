import Foundation

/// Описание обьекта продукта,
/// получаемого при сетевом запросе перечня товаров
struct Reviews: Decodable, Hashable {
    let idProduct: Int
    let reviews: [ReviewUser]

    enum CodingKeys: String, CodingKey {
        case idProduct = "id_product"
        case reviews
    }
}

struct ReviewUser: Decodable, Hashable {
    let nameUser: String
    let idComment: Int
    let text: String

    enum CodingKeys: String, CodingKey {
        case nameUser = "name_user"
        case idComment = "id_comment"
        case text
    }
}
