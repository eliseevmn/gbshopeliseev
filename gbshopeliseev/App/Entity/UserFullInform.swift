import Foundation

/// Объект пользователя с полной информации о нем,
/// Используется при регистрации и изменении данных о нем
struct UserFullInform: Decodable {
    let userId: String
    let login: String?
    let password: String
    let email: String
    let gender: String?
    let creditCard: String?
    let bio: String?

    enum CodingKeys: String, CodingKey {
        case userId = "id_user"
        case login = "username"
        case password
        case email
        case gender
        case creditCard = "credit_card"
        case bio
    }
}
