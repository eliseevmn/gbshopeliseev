import Foundation

struct UserProfile {
    let userId: Int?
    let name: String?
    let email: String?

    init(data: [String: Any]) {
        let userId = data["id"] as? Int
        let name = data["name"] as? String
        let email = data["email"] as? String

        self.userId = userId
        self.name = name
        self.email = email
    }
}

struct CurrentUser {
    let uid: String?
    let name: String?
    let email: String?

    init?(uid: String, data: [String: Any]) {
        guard
            let name = data["name"] as? String,
            let email = data["email"] as? String else { return nil }

        self.uid = uid
        self.name = name
        self.email = email
    }
}
