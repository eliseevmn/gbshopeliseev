import UIKit

class MainTabBarController: UITabBarController {

    // MARK: - Properties
    var coordinator: TabBarCoordinator?

    // MARK: - ViewController lyfecycle
    override func viewDidLoad() {
        super.viewDidLoad()

        viewControllers = [
            createNavController(viewController: CatalogViewController(), title: "Продукты", imageName: "products"),
            createNavController(viewController: BasketViewController(), title: "Корзина", imageName: "basket"),
            createNavController(viewController: ProfileViewController(), title: "Профиль", imageName: "icon")
        ]
    }

    // MARK: - Private functions
    private func createNavController(viewController: UIViewController,
                                     title: String,
                                     imageName: String) -> UIViewController {
        let navController = UINavigationController(rootViewController: viewController)
        viewController.navigationItem.title = title
        viewController.view.backgroundColor = .white
        navController.tabBarItem.title = title
        navController.tabBarItem.image = UIImage(named: imageName)
        return navController
    }
}
