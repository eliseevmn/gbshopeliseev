import UIKit

class CatalogItemCell: UICollectionViewCell {

    // MARK: - Properties
    static let reuserId = "ProductItemCell"

    // MARK: - Outlets
    let nameProduct: UILabel = {
        let label = UILabel()
        label.numberOfLines = 0
        label.font = UIFont.init(name: "avenir", size: 14)
        return label
    }()
    let idProduct: UILabel = {
        let label = UILabel()
        label.font = UIFont.init(name: "avenir", size: 14)
        return label
    }()
    let priceProduct: UILabel = {
        let label = UILabel()
        label.font = UIFont.init(name: "avenir", size: 14)
        return label
    }()
    let imageProduct: UIImageView = {
        let imageView = UIImageView()
        imageView.image = #imageLiteral(resourceName: "noimage")
        imageView.contentMode = .scaleAspectFit
        imageView.layer.masksToBounds = true
        imageView.heightAnchor.constraint(equalToConstant: 100).isActive = true
        return imageView
    }()

    override init(frame: CGRect) {
        super.init(frame: frame)
        configureUI()
        setupShadows()
        backgroundColor = .white
    }

    private func setupShadows() {
        self.layer.cornerRadius = 15.0
        self.layer.borderColor = UIColor.clear.cgColor
        self.layer.masksToBounds = true

        self.contentView.layer.cornerRadius = 15.0
        self.layer.shadowColor = UIColor.black.cgColor
        self.layer.shadowOffset = CGSize(width: 0, height: 0.0)
        self.layer.shadowRadius = 6.0
        self.layer.shadowOpacity = 0.6
        self.layer.cornerRadius = 15.0
        self.layer.masksToBounds = false
        self.layer.shadowPath = UIBezierPath(
            roundedRect: self.bounds,
            cornerRadius: self.contentView.layer.cornerRadius).cgPath
    }

    private func configureUI() {
        let verticalStackView = UIStackView(arrangedSubviews: [
            nameProduct, idProduct, priceProduct
        ])
        verticalStackView.axis = .vertical
        verticalStackView.spacing = 5
        let overlayStackView = UIStackView(arrangedSubviews: [
            imageProduct,
            verticalStackView
        ])
        verticalStackView.anchor(
            top: nil,
            leading: overlayStackView.leadingAnchor,
            bottom: overlayStackView.bottomAnchor,
            trailing: overlayStackView.trailingAnchor,
            padding: .init(top: 0, left: 10, bottom: 10, right: 10))
        overlayStackView.axis = .vertical
        overlayStackView.spacing = 10
        overlayStackView.distribution = .fillEqually
        addSubview(overlayStackView)
        overlayStackView.fillSuperview()
    }

    func configureCell(product: ProductItem) {
        nameProduct.text = "Название продукта \n" + product.productName
        idProduct.text = "Код продукта: \(product.productId)"
        priceProduct.text = "Цена: \(product.productPrice) руб."
    }

    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
