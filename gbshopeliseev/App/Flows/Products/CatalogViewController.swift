import UIKit

// Enum описывает тип секции
enum Section: CaseIterable {
    case products
}

// Enum описывает тип ячеек в секциях
enum GroupType: Hashable {
    case productsGroup(ProductItem)
}

// Класс описывает список продуктов
class CatalogViewController: UIViewController {

    // MARK: - Dependency
    var dataSource: UICollectionViewDiffableDataSource<Section, GroupType>?
    private let productService = NetworkServiceFactory.shared.makeProductsRequestFactory()

    // MARK: - Properties
    var products = [ProductItem]()

    // MARK: - Outlets
    var collectionView: UICollectionView!

    // MARK: - ViewController lyfecycle
    override func viewDidLoad() {
        super.viewDidLoad()
        view.backgroundColor = .yellow
        setupCollectionView()
        createDataSource()
        setupSearchBar()
        fetchingProducts()
    }

    // MARK: - Private functions
    private func setupCollectionView() {
        collectionView = UICollectionView(frame: view.bounds, collectionViewLayout: createCompositionalLayout())
        collectionView.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        collectionView.backgroundColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
        collectionView.delegate = self
        view.addSubview(collectionView)

        collectionView.register(CatalogItemCell.self, forCellWithReuseIdentifier: CatalogItemCell.reuserId)
    }

    private func fetchingProducts() {
        productService.getProductList(pageNumber: 1, categoryId: 123) { (products) in
            guard let products = products else { return }
            self.products = products
            print(products.count)
            var snapshot = NSDiffableDataSourceSnapshot<Section, GroupType>()
            snapshot.appendSections([.products])
            products.forEach { (productItem) in
                snapshot.appendItems([.productsGroup(productItem)], toSection: .products)
            }
            self.dataSource?.apply(snapshot)
        }
    }

    private func setupSearchBar() {
        navigationController?.navigationBar.barTintColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
        navigationController?.navigationBar.shadowImage = UIImage()
        let searchController = UISearchController(searchResultsController: nil)
        navigationItem.searchController = searchController
        navigationItem.hidesSearchBarWhenScrolling = false
        searchController.hidesNavigationBarDuringPresentation = false
        searchController.obscuresBackgroundDuringPresentation = false
        searchController.searchBar.delegate = self
    }

    private func reloadData(with searchText: String?) {
        let filtered = products.filter { (product) -> Bool in
            product.contains(filter: searchText)
        }

        var snapshot = NSDiffableDataSourceSnapshot<Section, GroupType>()
        snapshot.appendSections([.products])
        filtered.forEach { (filteredItem) in
            snapshot.appendItems([.productsGroup(filteredItem)], toSection: .products)
        }
        dataSource?.apply(snapshot, animatingDifferences: true)
    }

    // MARK: - Manage the data in UI COmpositional layout
    func createDataSource() {
        dataSource = UICollectionViewDiffableDataSource<Section, GroupType>(
            collectionView: collectionView,
            cellProvider: { (collectionView, indexPath, groupType) -> UICollectionViewCell? in

            switch groupType {
            case .productsGroup(let product):
                let cell = collectionView.dequeueReusableCell(
                    withReuseIdentifier: CatalogItemCell.reuserId,
                    for: indexPath) as? CatalogItemCell
                cell?.configureCell(product: product)
                return cell
            }
        })
    }

    private func createCompositionalLayout() -> UICollectionViewLayout {
        let layout = UICollectionViewCompositionalLayout { (sectionIndex, _) -> NSCollectionLayoutSection? in
            let section = Section.allCases[sectionIndex]
            switch section {
            case .products:
                return self.createProductsSection()
            }
        }
        return layout
    }

    func createProductsSection() -> NSCollectionLayoutSection {
        let itemSize = NSCollectionLayoutSize(widthDimension: .fractionalWidth(1),
                                              heightDimension: .fractionalHeight(1))
        let item = NSCollectionLayoutItem(layoutSize: itemSize)

        let groupSize = NSCollectionLayoutSize(widthDimension: .fractionalWidth(1.0),
                                               heightDimension: .fractionalWidth(0.6))
        let group = NSCollectionLayoutGroup.horizontal(layoutSize: groupSize, subitem: item, count: 2)
        let spacing = CGFloat(15)
        group.interItemSpacing = .fixed(spacing)

        let section = NSCollectionLayoutSection(group: group)
        section.interGroupSpacing = spacing
        section.contentInsets = NSDirectionalEdgeInsets.init(top: 15, leading: 15, bottom: 15, trailing: 15)
        return section
    }
}

// MARK: - UICollecetionViewDelegate
extension CatalogViewController: UICollectionViewDelegate {

    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let product = products[indexPath.item]
        let controller = ProductViewController()
        controller.idProduct = product.productId
        navigationController?.pushViewController(controller, animated: true)
    }
}

// MARK: - UISearchBarDelegate
extension CatalogViewController: UISearchBarDelegate {
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        reloadData(with: searchText)
    }

    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        var snapshot = NSDiffableDataSourceSnapshot<Section, GroupType>()
        snapshot.appendSections([.products])
        products.forEach { (productItem) in
            snapshot.appendItems([.productsGroup(productItem)], toSection: .products)
        }
        self.dataSource?.apply(snapshot)
    }
}
