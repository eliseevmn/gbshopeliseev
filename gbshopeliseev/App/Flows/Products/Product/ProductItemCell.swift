import UIKit

class ProductItemCell: UITableViewCell {

    // MARK: - Properties
    static let reuserId = "ProductItemCell"

    // MARK: - Outlets
    let imageProduct: UIImageView = {
        let imageView = UIImageView()
        imageView.image = #imageLiteral(resourceName: "noimage")
        imageView.contentMode = .scaleAspectFit
        imageView.layer.masksToBounds = true
        imageView.heightAnchor.constraint(equalToConstant: 300).isActive = true
        return imageView
    }()

    let nameProduct: UILabel = {
        let label = UILabel()
        label.numberOfLines = 0
        label.font = UIFont.init(name: "avenir", size: 20)
        label.textAlignment = .left
        return label
    }()

    let priceProduct: UILabel = {
        let label = UILabel()
        label.numberOfLines = 0
        label.font = UIFont.init(name: "avenir", size: 20)
        label.textAlignment = .right
        return label
    }()

    let descriptionProduct: UILabel = {
        let label = UILabel()
        label.numberOfLines = 0
        label.font = UIFont.init(name: "avenir", size: 20)
        return label
    }()

    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        configureUI()
    }

    private func configureUI() {
        let horizontalStackView = UIStackView(arrangedSubviews: [
            nameProduct, priceProduct
        ])
        let verticalSTackView = UIStackView(arrangedSubviews: [
            imageProduct, horizontalStackView, descriptionProduct
        ])
        verticalSTackView.axis = .vertical
        verticalSTackView.spacing = 10
        addSubview(verticalSTackView)
        verticalSTackView.fillSuperview()
    }

    func setupCell(product: ProductById?) {
        nameProduct.text = product?.productName
        priceProduct.text = "\(product?.productPrice ?? 0)"
    }

    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
