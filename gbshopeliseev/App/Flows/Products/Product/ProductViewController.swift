import UIKit

class ProductViewController: UIViewController {

    // MARK: Properties
    var idProduct: Int = 0
    var product: ProductById?

    // MARK: - Dependency
    private let productService = NetworkServiceFactory.shared.makeProductsRequestFactory()
    private let basketService = NetworkServiceFactory.shared.makeBasketRequestFactory()
    private let reviewService = NetworkServiceFactory.shared.makeReviewRequestFactory()

    // MARK: - Outlets
    var tableView: UITableView!

    let addToBasketButton: UIButton = {
        let button = UIButton()
        button.setTitle("Добавить в корзину", for: .normal)
        button.heightAnchor.constraint(equalToConstant: 50).isActive = true
        button.backgroundColor = #colorLiteral(red: 0.1411764771, green: 0.3960784376, blue: 0.5647059083, alpha: 1)
        button.setTitleColor(.white, for: .normal)
        button.titleLabel?.font = UIFont.boldSystemFont(ofSize: 16)
        button.layer.cornerRadius = 25/2
        button.layer.masksToBounds = true
        button.addTarget(self, action: #selector(handleAddToBasket), for: .touchUpInside)
        return button
    }()

    let addRewiewButton: UIButton = {
        let button = UIButton()
        button.setTitle("Оставить отзыв", for: .normal)
        button.heightAnchor.constraint(equalToConstant: 50).isActive = true
        button.backgroundColor = #colorLiteral(red: 0.1411764771, green: 0.3960784376, blue: 0.5647059083, alpha: 1)
        button.setTitleColor(.white, for: .normal)
        button.titleLabel?.font = UIFont.boldSystemFont(ofSize: 16)
        button.layer.cornerRadius = 25/2
        button.layer.masksToBounds = true
        button.addTarget(self, action: #selector(handleAddRewiew), for: .touchUpInside)
        return button
    }()

    // MARK: - ViewControllers lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()
        setupTableView()
        configureUI()
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        fetchingProduct()
        fetchingReviews()
    }

    // MARK: - Private functions
    private func setupTableView() {
        tableView = UITableView.init(frame: .zero, style: .grouped)
        tableView.backgroundColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
        tableView.delegate = self
        tableView.dataSource = self

        tableView.register(ProductItemCell.self, forCellReuseIdentifier: ProductItemCell.reuserId)
    }

    private func fetchingProduct() {
        productService.getProductItem(productId: idProduct) { (product) in
            guard let product = product else {
                return
            }
            self.product = product
            DispatchQueue.main.async {
                self.tableView.reloadData()
            }
        }
    }

    private func fetchingReviews() {
        reviewService.getReviews(idProduct: 111) { (reviews) in
            guard let reviews = reviews else {
                return
            }
            print(reviews)
        }
    }

    private func configureUI() {
        view.backgroundColor = .white
        let verticalStackView = UIStackView(arrangedSubviews: [
            tableView, addToBasketButton
        ])
        verticalStackView.axis = .vertical
        verticalStackView.spacing = 20
        view.addSubview(verticalStackView)
        verticalStackView.anchor(
            top: view.safeAreaLayoutGuide.topAnchor,
            leading: view.leadingAnchor,
            bottom: view.safeAreaLayoutGuide.bottomAnchor,
            trailing: view.trailingAnchor,
            padding: .init(top: 10, left: 16, bottom: 10, right: 16))
    }

    @objc private func handleAddToBasket() {
        let alertController = UIAlertController(title: nil,
                                                message: "Добавить в корзину?",
                                                preferredStyle: .alert)
        let okAction = UIAlertAction(title: "Добавить", style: .default) { (_) in
            self.basketService.addToBasket(idProduct: 1, quantity: 1) { (result) in
                guard let result = result else {
                    return
                }
                print(result)

                if result.result == 1 {
                    self.addToBasketButton.isEnabled = false
                    self.addToBasketButton.alpha = 0.5
                    self.addToBasketButton.setTitle("Добавлено в корзину", for: .normal)
                }
            }
        }
        let cancelAction = UIAlertAction(title: "Cancel", style: .cancel, handler: nil)
        alertController.addAction(okAction)
        alertController.addAction(cancelAction)
        present(alertController, animated: true, completion: nil)
    }

    @objc private func handleAddRewiew() {
        reviewService.addReview(idUser: 111, text: "Отзыв") { (result) in
            guard let result = result else {
                return
            }
            print(result)
        }
    }
}

// MARK: - UIUICollectionViewDelegate, UICollectionViewDataSource
extension ProductViewController: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: ProductItemCell.reuserId, for: indexPath) as? ProductItemCell else {
            fatalError("Проблема загрузки ячеек")
        }
        cell.setupCell(product: product)
        return cell
    }
}
