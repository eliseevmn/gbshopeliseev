import UIKit

class BasketViewController: UIViewController {

    // MARK: Properties
    var idProduct: Int = 0
    var products: [ProductById] = []

    // MARK: - Dependency
    private let basketService = NetworkServiceFactory.shared.makeBasketRequestFactory()

    // MARK: - Outlets
    var tableView: UITableView!

    let buyButton: UIButton = {
        let button = UIButton()
        button.setTitle("Оплатить товар", for: .normal)
        button.heightAnchor.constraint(equalToConstant: 50).isActive = true
        button.backgroundColor = #colorLiteral(red: 0.1411764771, green: 0.3960784376, blue: 0.5647059083, alpha: 1)
        button.setTitleColor(.white, for: .normal)
        button.titleLabel?.font = UIFont.boldSystemFont(ofSize: 16)
        button.layer.cornerRadius = 25/2
        button.layer.masksToBounds = true
        button.addTarget(self, action: #selector(handleBuyFromBasket), for: .touchUpInside)
        return button
    }()

    // MARK: - ViewControllers lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()
        setupTableView()
        configureUI()
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        fetchingBasket()
    }

    // MARK: - Private functions
    private func setupTableView() {
        tableView = UITableView.init(frame: .zero, style: .grouped)
        tableView.backgroundColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
        tableView.delegate = self
        tableView.dataSource = self

        tableView.register(BasketCell.self, forCellReuseIdentifier: BasketCell.reuserId)
    }

    private func fetchingBasket() {
        basketService.getBasket(idUser: 1) { (result) in
            guard let result = result else {
                return
            }
            print(result)
        }
    }

    private func configureUI() {
        view.backgroundColor = .white
        let verticalStackView = UIStackView(arrangedSubviews: [
            tableView, buyButton
        ])
        verticalStackView.axis = .vertical
        verticalStackView.spacing = 20
        view.addSubview(verticalStackView)
        verticalStackView.anchor(
            top: view.safeAreaLayoutGuide.topAnchor,
            leading: view.leadingAnchor,
            bottom: view.safeAreaLayoutGuide.bottomAnchor,
            trailing: view.trailingAnchor,
            padding: .init(top: 10, left: 16, bottom: 10, right: 16))
    }

    @objc private func handleBuyFromBasket() {
        let alertController = UIAlertController(title: nil,
                                                message: "Купить товар",
                                                preferredStyle: .alert)
        let okAction = UIAlertAction(title: "Купить", style: .default) { (_) in
            self.basketService.buyProductFromBasket(idUser: 111) { (result) in
                guard let result = result else {
                    return
                }
                print(result)
            }
        }
        let cancelAction = UIAlertAction(title: "Cancel", style: .cancel, handler: nil)
        alertController.addAction(okAction)
        alertController.addAction(cancelAction)
        present(alertController, animated: true, completion: nil)
    }
}

// MARK: - UIUICollectionViewDelegate, UICollectionViewDataSource
extension BasketViewController: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return products.count
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: BasketCell.reuserId, for: indexPath) as? BasketCell else {
            fatalError("Проблема загрузки ячеек")
        }
        let product = products[indexPath.row]
        cell.setupCell(product: product)
        return cell
    }
}
