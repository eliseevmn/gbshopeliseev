import UIKit
import FBSDKLoginKit
import FirebaseAuth
import FirebaseDatabase
import FirebaseCrashlytics
import GoogleSignIn

/// Экран авторизации
class AuthViewController: UIViewController {

    // MARK: - Dependency
    private let authService = NetworkServiceFactory.shared.makeAuthRequestFactory()

    // MARK: - Properties
    var userProfile: UserProfile?
    var coordinator: MainCoordinator?

    // MARK: - Outlets
    let leftBorderView: UIView = {
        let view = CustomBorderAuthView()
        return view
    }()

    let rigthBorderView: UIView = {
        let view = CustomBorderAuthView()
        return view
    }()

    let borderLabel: UILabel = {
        let label = CustomAuthLabel(title: "ИЛИ", fontSize: 16, fontWeight: .medium, color: #colorLiteral(red: 0.1019607857, green: 0.2784313858, blue: 0.400000006, alpha: 1))
        return label
    }()

    let titleLabel: UILabel = {
        let label = CustomAuthLabel(title: "Экран авторизации", fontSize: 26, fontWeight: .bold, color: .black)
        return label
    }()

    let imageView: UIImageView = {
        let imageView = UIImageView()
        imageView.image = #imageLiteral(resourceName: "tampa")
        imageView.contentMode = .scaleAspectFit
        imageView.layer.cornerRadius = 6
        imageView.layer.masksToBounds = true
        imageView.heightAnchor.constraint(equalToConstant: 100).isActive = true
        return imageView
    }()

    let activityIndicator: UIActivityIndicatorView = {
        let activityIndicator = UIActivityIndicatorView(style: .large)
        activityIndicator.color = #colorLiteral(red: 0.521568656, green: 0.1098039225, blue: 0.05098039284, alpha: 1)
        return activityIndicator
    }()

    let emailTextField: UITextField = {
        let textfield = CustomTextField(cornerRadius: 10, height: 50, fontSize: 16, labelText: "Введите свой email")
        textfield.addTarget(self, action: #selector(textFieldChange), for: .editingChanged)
        textfield.accessibilityIdentifier = "emailTextField"
        return textfield
    }()

    let passwordTextField: UITextField = {
        let textfield = CustomTextField(cornerRadius: 10, height: 50, fontSize: 16, labelText: "Введите свой пароль")
        textfield.addTarget(self, action: #selector(textFieldChange), for: .editingChanged)
        textfield.isSecureTextEntry = true
        textfield.accessibilityIdentifier = "passwordTextField"
        return textfield
    }()

    let signInButton: UIButton = {
        let button = CustomAuthButton(title: "ВОЙТИ")
        button.addTarget(self, action: #selector(handleSignInWithEmail), for: .touchUpInside)
        return button
    }()

    let notRegisterButton: UIButton = {
        let button = UIButton(type: .system)
        button.setTitle("Не зарегистрированы?", for: .normal)
        button.addTarget(self, action: #selector(handleToSignUpController), for: .touchUpInside)
        return button
    }()

    lazy var fbLoginButtonCustom: UIButton = {
        let button = CustomProviderButton(title: "Facebook", image: #imageLiteral(resourceName: "facebook"))
        button.backgroundColor = #colorLiteral(red: 0.1411764771, green: 0.3960784376, blue: 0.5647059083, alpha: 1)
        button.addTarget(self, action: #selector(handleFacebookLogin), for: .touchUpInside)
        return button
    }()

    lazy var googleButtonCustom: UIButton = {
        let button = CustomProviderButton(title: "Google", image: #imageLiteral(resourceName: "google"))
        button.backgroundColor = .gray
        button.addTarget(self, action: #selector(handleGoogleLogin), for: .touchUpInside)
        return button
    }()

    // MARK: - ViewController lyfecycle
    override func viewDidLoad() {
        super.viewDidLoad()
        GIDSignIn.sharedInstance()?.delegate = self
        GIDSignIn.sharedInstance()?.presentingViewController = self
        configureUI()
        setContinueSignButton(enabled: false)

        let hideKeyboardGesture = UITapGestureRecognizer(target: self, action: #selector(hideKeyboard))
        view.addGestureRecognizer(hideKeyboardGesture)
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        navigationController?.navigationBar.isHidden = true
        cleaTextFields()
    }

    // MARK: - Configure UI
    private func configureUI() {
        view.backgroundColor = .white
        let providerStackView = UIStackView(arrangedSubviews: [
            fbLoginButtonCustom, googleButtonCustom
        ])
        providerStackView.distribution = .fillEqually
        providerStackView.spacing = 10

        let borderStackView = UIStackView(arrangedSubviews: [
            rigthBorderView, borderLabel, leftBorderView
        ])
        borderStackView.alignment = .center
        borderStackView.distribution = .fillEqually

        let bottomAuthStackView = UIStackView(arrangedSubviews: [
            borderStackView,
            providerStackView
        ])
        bottomAuthStackView.axis = .vertical
        view.addSubview(bottomAuthStackView)
        bottomAuthStackView.spacing = 40
        bottomAuthStackView.anchor(top: nil,
                                 leading: view.leadingAnchor,
                                 bottom: view.bottomAnchor,
                                 trailing: view.trailingAnchor,
                                 padding: .init(top: 0, left: 32, bottom: 40, right: 32))

        let registrationsStackView = UIStackView(arrangedSubviews: [
            emailTextField, passwordTextField, signInButton, notRegisterButton
        ])
        registrationsStackView.axis = .vertical
        registrationsStackView.spacing = 30

        let overlayStackView = UIStackView(arrangedSubviews: [
            titleLabel, imageView, registrationsStackView
        ])
        view.addSubview(overlayStackView)
        overlayStackView.axis = .vertical
        overlayStackView.spacing = 30
        overlayStackView.anchor(top: view.topAnchor,
                                leading: view.leadingAnchor,
                                bottom: nil,
                                trailing: view.trailingAnchor,
                                padding: .init(top: 60, left: 48, bottom: 0, right: 48))

        view.addSubview(activityIndicator)
        activityIndicator.centerInSuperview()
    }

    // MARK: - Register functions with Email and Firebase functions
    @objc private func handleSignInWithEmail() {
//        fatalError()
        activityIndicator.startAnimating()
        guard let email = emailTextField.text,
            let password = passwordTextField.text else {
                return
        }
        authService.login(userName: email, password: password) { [weak self] (user) in
            guard user != nil else {
                self?.showAlert(title: "Ошибка", message: "Сервер приложения недоступен!")
                self?.activityIndicator.stopAnimating()
                return
            }

            self?.signInUser(email: email, password: password) { [weak self] (result) in
                switch result {
                case .success:
                    self?.activityIndicator.stopAnimating()
                    self?.coordinator?.toMainTabBarController()
                case .failure(let error):
                    self?.activityIndicator.stopAnimating()
                    self?.showAlert(title: "Ошибка", message: error.localizedDescription)
                }
            }
        }
    }

    private func signInUser(email: String?,
                            password: String?,
                            completion: @escaping(AuthResult) -> Void) {
        guard Validators.isFillled(login: emailTextField.text, password: passwordTextField.text) else {
            completion(.failure(AuthError.notFill))
            return
        }

        guard let password = password, let email = email else {
            completion(.failure(AuthError.unknownError))
            return
        }

        Auth.auth().signIn(withEmail: email, password: password) { (_, error) in
            if error != nil {
                completion(.failure(error!))
            } else {
                completion(.success)
            }
        }
    }

    // MARK: - Private functions
    @objc private func handleToSignUpController() {
        self.coordinator?.toSignUpController()
    }

    @objc private func textFieldChange() {
        guard let email = emailTextField.text,
            let password = passwordTextField.text
            else { return }
        let formFilled = !(email.isEmpty) && !(password.isEmpty)
        setContinueSignButton(enabled: formFilled)
    }

    private func setContinueSignButton(enabled: Bool) {
        if enabled {
            signInButton.alpha = 1.0
            signInButton.isEnabled = true
        } else {
            signInButton.alpha = 0.5
            signInButton.isEnabled = false
        }
    }

    private func checkValid() -> String? {
        if emailTextField.text == "" ||
            passwordTextField.text == "" ||
            emailTextField.text == nil ||
            passwordTextField.text == nil {
            return "Please fill in all fields"
        }
        return nil
    }

    private func cleaTextFields() {
        emailTextField.text = ""
        passwordTextField.text = ""
    }

    @objc private func hideKeyboard() {
        view.endEditing(true)
    }
}

// MARK: - Facebook and Firebase functions
extension AuthViewController {

    @objc private func handleFacebookLogin() {
        authService.loginWithProvider { (result) in
            guard result != nil else {
                self.showAlert(title: "Ошибка", message: "Сервер приложения недоступен!")
                return
            }
            LoginManager().logIn(permissions: [ "email", "public_profile"], from: self) { (result, error) in
                if let error = error {
                    print(error.localizedDescription)
                    return
                }
                guard let result = result else { return }
                if result.isCancelled {
                    return
                } else {
                    self.signIntoToFirebase()
                }
            }
        }
    }

    private func signIntoToFirebase() {
        activityIndicator.startAnimating()
        let accessToken = AccessToken.current
        guard let accessTokenString = accessToken?.tokenString else { return }
        let credentials = FacebookAuthProvider.credential(withAccessToken: accessTokenString)
        Auth.auth().signIn(with: credentials) { (_, error) in
            if let error = error {
                print("Что-то пошло не так при регистрации через facebook", error)
                return
            }
            print("Successfully logged in with our FB user")
            self.fetchFacebookFields()
        }
    }

    private func fetchFacebookFields() {
        GraphRequest(graphPath: "me", parameters: ["fields": "id, name, email"]).start { (_, result, error) in
            if let error = error {
                print(error)
                return
            }
            if let userData = result as? [String: Any] {
                self.userProfile = UserProfile(data: userData)
                self.saveIntoFirebase()
            }
        }
    }

    private func saveIntoFirebase() {
        guard let uid = Auth.auth().currentUser?.uid else { return }
        let userData = ["name": userProfile?.name, "email": userProfile?.email]
        let values = [uid: userData]
        Database.database().reference().child("users").updateChildValues(values) { (error, _) in
            if let error = error {
                print(error)
                return
            }
            print("Successfully saved user into firebase")
            self.activityIndicator.stopAnimating()
            self.coordinator?.toMainTabBarController()
        }
    }
}

// MARK: - Google SDK and Firebase functions
extension AuthViewController: GIDSignInDelegate {

    @objc private func handleGoogleLogin() {
        authService.loginWithProvider { (result) in
            guard result != nil else {
                  self.activityIndicator.stopAnimating()
                  self.showAlert(title: "Ошибка", message: "Сервер приложения недоступен!")
                  return
            }
            GIDSignIn.sharedInstance()?.signIn()
        }
    }

    func sign(_ signIn: GIDSignIn!, didSignInFor user: GIDGoogleUser!, withError error: Error!) {
        activityIndicator.startAnimating()
        if let error = error {
            print("Failed log in to Google", error)
            return
        }
        print("Successfullly liged into Google")

        if let userName = user.profile.name, let userEmail = user.profile.email {
            let userData = ["name": userName, "email": userEmail]
            userProfile = UserProfile(data: userData)
        }

        guard let authntication = user.authentication else { return }
        let credential = GoogleAuthProvider.credential(
            withIDToken: authntication.idToken,
            accessToken: authntication.accessToken)
        Auth.auth().signIn(with: credential) { (_, error) in
            if let error = error {
                print("Something went wrong with our Google user: ", error)
                return
            }

            print("Succesffully loggedinto firebase with google")
            self.saveIntoFirebase()
        }
    }
}
