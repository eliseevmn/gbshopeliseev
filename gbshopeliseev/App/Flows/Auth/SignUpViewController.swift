import UIKit
import Firebase
import FirebaseAuth

class SignUpViewController: UIViewController {

    // MARK: - Properties
    var coordinator: MainCoordinator?

    // MARK: - Dependency
    private let authService = NetworkServiceFactory.shared.makeAuthRequestFactory()

    // MARK: - Outlets
    let titleLabel: UILabel = {
        let label = UILabel()
        label.text = "Экран регистрации"
        label.font = UIFont.boldSystemFont(ofSize: 26)
        label.textAlignment = .center
        return label
    }()

    var activityIndicator: UIActivityIndicatorView = {
        let activityIndicator = UIActivityIndicatorView(style: .large)
        activityIndicator.color = #colorLiteral(red: 0.521568656, green: 0.1098039225, blue: 0.05098039284, alpha: 1)
        return activityIndicator
    }()

    let userNameTextField: UITextField = {
        let textfield = CustomTextField(cornerRadius: 10, height: 50, fontSize: 16, labelText: "Введите ваш логин")
        return textfield
    }()

    let emailTextField: UITextField = {
        let textfield = CustomTextField(cornerRadius: 10, height: 50, fontSize: 16, labelText: "Введите ваш email")
        return textfield
    }()

    let passwordTextField: UITextField = {
        let textfield = CustomTextField(cornerRadius: 10, height: 50, fontSize: 16, labelText: "Введите ваш пароль")
        textfield.isSecureTextEntry = true
        return textfield
    }()

    let conformPasswordTextField: UITextField = {
        let textfield = CustomTextField(cornerRadius: 10, height: 50, fontSize: 16, labelText: "Подтвердите ваш пароль")
        textfield.isSecureTextEntry = true
        return textfield
    }()

    let registerButton: UIButton = {
        let button = CustomAuthButton(title: "Зарегистрироваться")
        button.addTarget(self, action: #selector(handleLogin), for: .touchUpInside)
        return button
    }()

    // MARK: - ViewController lyfecycle
    override func viewDidLoad() {
        super.viewDidLoad()

        navigationController?.navigationBar.isHidden = false
        view.backgroundColor = .white
        configureUI()
    }

    // MARK: - Configure UI
    private func configureUI() {
        let verticalStackView = UIStackView(arrangedSubviews: [
            userNameTextField,
            emailTextField,
            passwordTextField,
            conformPasswordTextField
        ])
        verticalStackView.axis = .vertical
        verticalStackView.spacing = 20

        let overlayStackView = UIStackView(arrangedSubviews: [
            titleLabel, verticalStackView, registerButton
        ])
        view.addSubview(overlayStackView)
        overlayStackView.anchor(top: view.safeAreaLayoutGuide.topAnchor,
                                leading: view.leadingAnchor,
                                bottom: nil,
                                trailing: view.trailingAnchor,
                                padding: .init(top: 30, left: 40, bottom: 0, right: 40))
        overlayStackView.axis = .vertical
        overlayStackView.spacing = 35

        view.addSubview(activityIndicator)
        activityIndicator.centerInSuperview()
    }

    // MARK: - Register functions with Firebase
    @objc private func handleLogin() {
        registerToOwnServer()

    }

    private func setContinueSignButton(enabled: Bool) {
        if enabled {
            registerButton.alpha = 1.0
            registerButton.isEnabled = true
        } else {
            registerButton.alpha = 0.5
            registerButton.isEnabled = false
        }
    }

    private func registerToOwnServer() {
        activityIndicator.startAnimating()
        guard let login = userNameTextField.text,
            let password = passwordTextField.text,
            let email = emailTextField.text
            else { return }
        let uuid = UUID().uuidString
        let userFullInform = UserFullInform(userId: uuid,
                                            login: login,
                                            password: password,
                                            email: email,
                                            gender: "",
                                            creditCard: "",
                                            bio: "")
        authService.registerUser(user: userFullInform) { (result) in
            guard result != nil else {
                self.showAlert(title: "Ошибка", message: "Сервер недоступен")
                self.activityIndicator.stopAnimating()
                return
            }
            self.registerUserToFirebase(userName: userFullInform.login,
                                        email: userFullInform.email,
                                        password: userFullInform.password) { (result) in
                switch result {
                case .success:
                    self.activityIndicator.stopAnimating()
                    self.showAlert(title: "Успешно", message: "Вы зарегистрированы", completion: { (_) in
                        self.coordinator?.removeController()
                    })
                case .failure(let error):
                    self.activityIndicator.stopAnimating()
                    self.showAlert(title: "Ошибка", message: error.localizedDescription)
                }
            }
        }
    }

    private func registerUserToFirebase(userName: String?,
                                        email: String?,
                                        password: String?,
                                        completion: @escaping(AuthResult) -> Void) {

        guard Validators.isFillled(login: userNameTextField.text, password: passwordTextField.text) else {
            completion(.failure(AuthError.notFill))
            setContinueSignButton(enabled: true)
            return
        }

        guard let password = password, let email = email else {
            completion(.failure(AuthError.unknownError))
            return
        }

        guard Validators.isSimpleEmail(email) else {
            completion(.failure(AuthError.invalidEmail))
            return
        }

        guard Validators.notConfirmPasswords(password: password, confirmPassword: conformPasswordTextField.text) else {
            completion(.failure(AuthError.confirmPassword))
            return
        }

        Auth.auth().createUser(withEmail: email, password: password) { (result, error) in
            guard result != nil else {
                completion(.failure(error!))
                return
            }
            if let changeRequest = Auth.auth().currentUser?.createProfileChangeRequest() {
                changeRequest.displayName = userName
                changeRequest.commitChanges { (error) in
                    if let error = error {
                        print(error.localizedDescription)
                    }
                    print("User display name changed!")
                }
            }
            print("Succesfully logged into Firebase")
            completion(.success)
        }
    }
}
