import UIKit
import FBSDKLoginKit
import FirebaseAuth
import FirebaseDatabase
import GoogleSignIn

class ProfileViewController: UIViewController {

    // MARK: - Dependency
    private let authService = NetworkServiceFactory.shared.makeAuthRequestFactory()

    // MARK: - Properties
    private var provider: String?
    private var currentUser: CurrentUser?
    private var user: UserFullInform?
    var coordinator: MainCoordinator?

    // MARK: - Outlets
    lazy var logoutButton: UIButton = {
        let button = UIButton()
        button.backgroundColor = #colorLiteral(red: 0.5667956471, green: 0.5676371455, blue: 0.5925787091, alpha: 1)
        button.setTitle("Log Out", for: .normal)
        button.titleLabel?.font = UIFont.boldSystemFont(ofSize: 16)
        button.setTitleColor(.white, for: .normal)
        button.layer.cornerRadius = 4
        button.addTarget(self, action: #selector(handleSignOut), for: .touchUpInside)
        button.frame = CGRect(x: 32,
                                   y: view.frame.height - 172,
                                   width: view.frame.width - 64,
                                   height: 50)
        return button
    }()

    let titleLabel: UILabel = {
        let label = UILabel()
        label.numberOfLines = 0
        return label
    }()

    let activityIndacator: UIActivityIndicatorView = {
        let activityIndicator = UIActivityIndicatorView()
        activityIndicator.startAnimating()
        activityIndicator.hidesWhenStopped = true
        return activityIndicator
    }()

    // MARK: - ViewController lyfecycle
    override func viewDidLoad() {
        super.viewDidLoad()

        navigationController?.navigationBar.isHidden = true
        view.backgroundColor = .gray
        setupViews()
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        fetchingUserData()
    }

    // MARK: - Configure UI
    private func setupViews() {
        view.addSubview(logoutButton)
        view.addSubview(titleLabel)
        view.addSubview(activityIndacator)
        titleLabel.centerInSuperview()
        titleLabel.isHidden = true
        activityIndacator.centerInSuperview()
    }

    // MARK: - Logout functions
    private func openAuthController() {
        do {
            try Auth.auth().signOut()
            self.tabBarController?.navigationController?.popViewController(animated: true)
        } catch let error {
            print("Failed to signout with error:", error.localizedDescription)
        }
    }
}

// MARK: - Fetching data from Firebase
extension ProfileViewController {

    private func fetchingUserData() {
        if Auth.auth().currentUser != nil {
            if let userName = Auth.auth().currentUser?.displayName {
                activityIndacator.stopAnimating()
                titleLabel.isHidden = false
                titleLabel.text = getProviderData(with: userName)
            } else {
                guard let uid = Auth.auth().currentUser?.uid else { return }
                Database.database().reference().child("users").child(uid)
                    .observeSingleEvent(of: .value, with: { (snapshot) in
                        guard let userData = snapshot.value as? [String: Any] else { return }

                        self.activityIndacator.stopAnimating()
                        self.titleLabel.isHidden = false

                        self.currentUser = CurrentUser(uid: uid, data: userData)
                        self.titleLabel.text = self.getProviderData(with: self.currentUser?.name ?? "Noname")
                    }, withCancel: { (error) in
                        print(error)
                })
            }
        }
    }

    @objc private func handleSignOut() {
        authService.logout(userId: 111) { (result) in
            guard result != nil else {
                self.showAlert(title: "Ошибка", message: "Сервер приложения недоступен!")
                return
            }

            if let providerData = Auth.auth().currentUser?.providerData {
                for userInfo in providerData {
                    switch userInfo.providerID {
                    case "facebook.com":
                        LoginManager().logOut()
                        print("User did log out of facebook")
                        self.openAuthController()
                    case "google.com":
                        GIDSignIn.sharedInstance()?.signOut()
                        print("User did log out of Google")
                        self.openAuthController()
                    case "password":
                        try? Auth.auth().signOut()
                        print("User did sign out")
                        self.openAuthController()
                    default:
                        print("User is signed in with \(userInfo.providerID)")
                    }
                }
            }
        }
    }

    private func getProviderData(with user: String) -> String {
        var greetings = ""
        if let providerData = Auth.auth().currentUser?.providerData {
            for userInfo in providerData {
                switch userInfo.providerID {
                case "facebook.com":
                    provider = "Fecabook"
                case "google.com":
                    provider = "Google"
                case "password":
                    provider = "Email"
                default:
                    break
                }
            }
            greetings = "\(user) Logged in with \(String(describing: provider))"
        }
        return greetings
    }
}
