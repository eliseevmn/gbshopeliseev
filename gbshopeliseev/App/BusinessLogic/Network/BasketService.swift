import Foundation

/// Отправление запросов, связанных с корзиной товаров
protocol BasketService {

    /// Добавление продукта в корзину
    /// - Parameters:
    ///   - idProduct: Идентификационный номер продукта
    ///   - quantity: Количество конкретного продукта
    func addToBasket(idProduct: Int, quantity: Int, completion: @escaping(CommonResult?) -> Void)

    /// Удаление продукта из корзины
    /// - Parameters:
    ///   - idProduct: Идентификационный номер продукта
    func deleteFromBasket(idProduct: Int, completion: @escaping(CommonResult?) -> Void)

    /// Получение корзины продуктов
    /// - Parameters:
    ///   - idUser: Идентификационный номер пользователя
    func getBasket(idUser: Int, completion: @escaping(CommonResult?) -> Void)

    /// Получение корзины продуктов
    /// - Parameters:
    ///   - idUser: Идентификационный номер пользователя
    func buyProductFromBasket(idUser: Int, completion: @escaping(CommonResult?) -> Void)
}

class BasketServiceImpl: BasketService {

    private var baseUrl: URL
    private let networkService: NetworkService

    init(baseUrl: URL, networkService: NetworkService) {
        self.baseUrl = baseUrl
        self.networkService = networkService
    }

    func addToBasket(idProduct: Int, quantity: Int, completion: @escaping (CommonResult?) -> Void) {
        let requestModel = Basket.AddToBasket(baseUrl: baseUrl, idProduct: idProduct, quantity: quantity)
        networkService.request(requestModel, completion: completion)
    }

    func deleteFromBasket(idProduct: Int, completion: @escaping (CommonResult?) -> Void) {
        let requestModel = Basket.DeleteFromBasket(baseUrl: baseUrl, idProduct: idProduct)
        networkService.request(requestModel, completion: completion)
    }

    func getBasket(idUser: Int, completion: @escaping (CommonResult?) -> Void) {
        let requestModel = Basket.GetBasket(baseUrl: baseUrl, idUser: idUser)
        networkService.request(requestModel, completion: completion)
    }

    func buyProductFromBasket(idUser: Int, completion: @escaping (CommonResult?) -> Void) {
        let requestModel = Basket.BuyBasket(baseUrl: baseUrl, idUser: idUser)
        networkService.request(requestModel, completion: completion)
    }
}
