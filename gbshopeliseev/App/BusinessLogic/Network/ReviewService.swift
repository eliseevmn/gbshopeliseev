import Foundation

/// Отправление запросов, связанных с отзывами на продукты
protocol ReviewService {

    /// Добавление отзыва на продукт
    /// - Parameters:
    ///   - idUser: Идентификационный номер пользователя
    ///   - text: Текст отзыва
    func addReview(idUser: Int, text: String, completion: @escaping (CommonResult?) -> Void)

    /// Одобрение отзыва на продукт
    /// - Parameters:
    ///   - idComment: Идентификационный номер отзыва
    func approveReview(idComment: Int, completion: @escaping (CommonResult?) -> Void)

    /// Удаление отзыва на продукт
    /// - Parameters:
    ///   - idComment: Идентификационный номер отзыва
    func removeReview(idComment: Int, completion: @escaping (CommonResult?) -> Void)

    /// Удаление отзыва на продукт
    /// - Parameters:
    ///   - idProduct: Идентификационный номер продукта
    func getReviews(idProduct: Int, completion: @escaping ([ReviewUser]?) -> Void)
}

class ReviewServiceImpl: ReviewService {

    private var baseUrl: URL
    private let networkService: NetworkService

    init(baseUrl: URL, networkService: NetworkService) {
        self.baseUrl = baseUrl
        self.networkService = networkService
    }

    func addReview(idUser: Int, text: String, completion: @escaping (CommonResult?) -> Void) {
        let requestModel = Review.AddReview(baseUrl: baseUrl, idUser: idUser, text: text)
        networkService.request(requestModel, completion: completion)
    }

    func approveReview(idComment: Int, completion: @escaping (CommonResult?) -> Void) {
        let requestModel = Review.ApproveReview(baseUrl: baseUrl, idComment: idComment)
        networkService.request(requestModel, completion: completion)
    }

    func removeReview(idComment: Int, completion: @escaping (CommonResult?) -> Void) {
        let requestModel = Review.RemoveReview(baseUrl: baseUrl, idComment: idComment)
        networkService.request(requestModel, completion: completion)
    }

    func getReviews(idProduct: Int, completion: @escaping ([ReviewUser]?) -> Void) {
        let requestModel = Review.GetReviews(baseUrl: baseUrl, idProduct: idProduct)
        networkService.request(requestModel) { (reviews: Reviews?) in
            completion(reviews?.reviews)
        }
    }
}
