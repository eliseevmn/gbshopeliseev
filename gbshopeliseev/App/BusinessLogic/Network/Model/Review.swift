import Foundation
import Alamofire

/// Описание моделей запросов на озывы к продуктам
class Review {

    /// Описание модели запроса на добаление продукта
    struct AddReview: RequestRouter {
        var baseUrl: URL
        var method: HTTPMethod = .post
        var path: String = "addReview"

        let idUser: Int
        let text: String
        var parameters: Parameters? {
            return [
                "id_user": idUser,
                "text": text
            ]
        }
    }

    /// Описание модели запроса на одобрение отзыва
    struct ApproveReview: RequestRouter {
        var baseUrl: URL
        var method: HTTPMethod = .post
        var path: String = "approveReview"

        let idComment: Int
        var parameters: Parameters? {
            return [
                "id_comment": idComment
            ]
        }
    }

    /// Описание модели запроса на удаление отзыва
    struct RemoveReview: RequestRouter {
        var baseUrl: URL
        var method: HTTPMethod = .delete
        var path: String = "removeReview"

        let idComment: Int
        var parameters: Parameters? {
            return [
                "id_comment": idComment
            ]
        }
    }

    /// Описание модели запроса на получение списка отзывов
    struct GetReviews: RequestRouter {
        let baseUrl: URL
        let method: HTTPMethod = .get
        let path: String = "getReviews"

        let idProduct: Int
        var parameters: Parameters? {
            return [
                "id_product": idProduct
            ]
        }
    }
}
