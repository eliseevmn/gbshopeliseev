import Foundation
import Alamofire

/// Описание модели запроса авторизации
class AuthUser {

    /// Описание модели запроса на вход в приложение
    struct Login: RequestRouter {
        let baseUrl: URL
        let method: HTTPMethod = .post
        let path: String = "login"

        let login: String
        let password: String
        var parameters: Parameters? {
            return [
                "username": login,
                "password": password
            ]
        }
    }

    /// Описание модели запроса на вход c помощью провайдеров
    struct LoginWithProvider: RequestRouter {
        let baseUrl: URL
        let method: HTTPMethod = .post
        let path: String = "loginWithProvider"
        var parameters: Parameters?
    }
}

extension AuthUser {

    /// Описанеи модели запроса выхода из приложения
    struct Logout: RequestRouter {
        let baseUrl: URL
        let method: HTTPMethod = .post
        let path: String = "logout"

        let userId: Int
        var parameters: Parameters? {
            return [
                "id_user": userId
            ]
        }
    }
}

extension AuthUser {

    /// Описание модели запроса на регистраицю
    struct RegusterUser: RequestRouter {
        let baseUrl: URL
        let method: HTTPMethod = .post
        let path: String = "register"
        let user: UserFullInform

        var parameters: Parameters? {
            return [
                "id_user": user.userId,
                "username": user.login ?? "",
                "password": user.password,
                "email": user.email,
                "gender": user.gender ?? "",
                "credit_card": user.creditCard ?? "",
                "bio": user.bio ?? ""
            ]
        }
    }
}

extension AuthUser {

    /// Описание модели на измнение данных пользователя
    struct ChangeUserData: RequestRouter {
        let baseUrl: URL
        let method: HTTPMethod = .post
        let path: String = "changeUserData"

        let userId: Int
        let login: String
        let password: String
        let email: String
        let gender: String
        let creditCard: String
        let bio: String
        var parameters: Parameters? {
            return [
                "id_user": userId,
                "username": login,
                "password": password,
                "email": email,
                "gender": gender,
                "credit_card": creditCard,
                "bio": bio
            ]
        }
    }
}
