import Foundation
import Alamofire

/// Отправление запросов, связанных с подуктами
protocol ProductService {

    /// Получение списка продуктов
    /// - Parameters:
    ///   - pageNumber: Номер страницы списка продуктов
    ///   - categoryId: Идентификационный номер категории продуктов
    func getProductList(pageNumber: Int, categoryId: Int, completionHandler: @escaping([ProductItem]?) -> Void)

    /// Получение информации по конкретному подукту
    /// - Parameters:
    ///   - productId: Идентификационный номер продукта
    func getProductItem(productId: Int, completionHandler: @escaping(ProductById?) -> Void)
}

class ProductServiceImpl: ProductService {

    private var baseUrl: URL
    private let networkService: NetworkService

    init(baseUrl: URL, networkService: NetworkService) {
        self.baseUrl = baseUrl
        self.networkService = networkService
    }
    func getProductList(pageNumber: Int, categoryId: Int, completionHandler: @escaping ([ProductItem]?) -> Void) {
        let requestModel = Products.GetProductList(baseUrl: baseUrl, pageNumber: pageNumber, idCategory: categoryId)
        networkService.request(requestModel) { (response: ProductsList?) in
            completionHandler(response?.products)
        }
    }

    func getProductItem(productId: Int, completionHandler: @escaping (ProductById?) -> Void) {
        let requestModel = Products.GetProduct(baseUrl: baseUrl, productId: productId)
        networkService.request(requestModel) { (response: ProductById?) in
            completionHandler(response)
        }
    }
}
