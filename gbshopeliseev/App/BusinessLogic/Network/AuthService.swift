import Foundation
import Alamofire

/// Отправление запросов, связаннных с авторизацией
protocol AuthService {

    /// Авторизация пользователя
    /// - Parameters:
    ///   - userName: Имя пользователя
    ///   - password: Пароль
    func login(userName: String, password: String, completionHandler: @escaping(User?) -> Void)

    /// Авторизация пользователя c помощью провайдеров
    func loginWithProvider(completionHandler: @escaping(CommonResult?) -> Void)

    /// Выход из приложения
    /// - Parameters:
    ///   - userId: идентификационный номер пользователя
    func logout(userId: Int, completionHandler: @escaping(CommonResult?) -> Void)

    /// Изменение данных пользователя

    /// Регистрация пользователя
    /// - Parameters:
    ///   - user: Пользователь с информацией о себе
    func registerUser(user: UserFullInform, completionHandler: @escaping(CommonResult?) -> Void)

    /// Изменени данных пользователя
    /// - Parameters:
    ///   - userId: Идентификационный номер пользователя
    ///   - userName: Имя пользователя
    ///   - password: Пароль
    ///   - email: Email пользователя
    ///   - gender: Пол польователя
    ///   - creditCard: Номер банковской карты
    ///   - bio: Информация о пользователе
    func changeUserData(userId: Int,
                        userName: String,
                        password: String,
                        email: String,
                        gender: String,
                        creditCard: String,
                        bio: String,
                        completionHandler: @escaping(CommonResult?) -> Void)
}

class AuthServiceImpl: AuthService {

    private var baseUrl: URL
    private let networkService: NetworkService

    init(baseUrl: URL, networkService: NetworkService) {
        self.baseUrl = baseUrl
        self.networkService = networkService
    }

    func login(userName: String, password: String, completionHandler: @escaping (User?) -> Void) {
        let requestModel = AuthUser.Login(baseUrl: baseUrl, login: userName, password: password)
        networkService.request(requestModel) { (response: LoginResult?) in
            completionHandler(response?.user)
        }
    }

    func loginWithProvider(completionHandler: @escaping (CommonResult?) -> Void) {
        let requestModel = AuthUser.LoginWithProvider(baseUrl: baseUrl)
        networkService.request(requestModel) { (response: CommonResult?) in
            completionHandler(response)
        }
    }

    func logout(userId: Int, completionHandler: @escaping (CommonResult?) -> Void) {
        let requestModel = AuthUser.Logout(baseUrl: baseUrl, userId: userId)
        networkService.request(requestModel) { (response: CommonResult?) in
            completionHandler(response)
        }
    }

    func registerUser(user: UserFullInform, completionHandler: @escaping (CommonResult?) -> Void) {
        let requesModel = AuthUser.RegusterUser(baseUrl: baseUrl, user: user)
        networkService.request(requesModel) { (response: CommonResult?) in
            completionHandler(response)
        }
    }

    func changeUserData(userId: Int,
                        userName: String,
                        password: String,
                        email: String,
                        gender: String,
                        creditCard: String,
                        bio: String,
                        completionHandler: @escaping (CommonResult?) -> Void) {
        let requestModel = AuthUser.ChangeUserData(baseUrl: baseUrl,
                                               userId: userId,
                                               login: userName,
                                               password: password,
                                               email: email,
                                               gender: gender,
                                               creditCard: creditCard,
                                               bio: bio)
        networkService.request(requestModel) { (response: CommonResult?) in
            completionHandler(response)
        }
    }
}
