import UIKit

class TabBarCoordinator: Coordinator {
    weak var parentCoordinator: MainCoordinator?
    var childCoordinators: [Coordinator] = []
    var navigationController: CustomNavigationController

    init(_ navigationController: CustomNavigationController) {
        self.navigationController = navigationController
    }

    func start() {
        let controller = MainTabBarController()
        controller.coordinator = self
        navigationController.pushViewController(controller, animated: true)
    }
}
