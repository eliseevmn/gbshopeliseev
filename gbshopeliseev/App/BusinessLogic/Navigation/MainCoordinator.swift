import UIKit

protocol Coordinator: class {
    var childCoordinators: [Coordinator] { get set }
    var navigationController: CustomNavigationController { get set }
    func start()
}

class MainCoordinator: NSObject, Coordinator {
    var childCoordinators = [Coordinator]()
    var navigationController: CustomNavigationController

    init(_ navigationController: CustomNavigationController) {
        self.navigationController = navigationController
    }

    func start() {
        let controller = AuthViewController()
        controller.coordinator = self
        navigationController.pushViewController(controller, animated: true)
    }

    func toSignUpController() {
        let controller = SignUpViewController()
        controller.coordinator = self
        navigationController.pushViewController(controller, animated: true)
    }

    func removeController() {
        navigationController.popViewController(animated: true)
    }

    func toMainTabBarController() {
        let child = TabBarCoordinator(navigationController)
        childCoordinators.append(child)
        child.parentCoordinator = self
        child.start()
    }

    func removeChild(coordinator child: Coordinator?) {
        childCoordinators.removeAll { $0 === child }
    }
}

extension MainCoordinator: UINavigationControllerDelegate {
    func navigationController(_ navigationController: UINavigationController,
                              didShow viewController: UIViewController,
                              animated: Bool) {
        guard
            let fromVC = navigationController.transitionCoordinator?.viewController(forKey: .from),
            !navigationController.viewControllers.contains(fromVC)
            else { return }

        if let tabBarVC = fromVC as? MainTabBarController {
            removeChild(coordinator: tabBarVC.coordinator)
        }
    }
}
