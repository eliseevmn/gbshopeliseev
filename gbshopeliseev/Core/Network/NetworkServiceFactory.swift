import UIKit
import Alamofire

// Фабрика по созданию сетевых запросов
class NetworkServiceFactory {

    static let shared: NetworkServiceFactory = NetworkServiceFactory()

    private(set) lazy var commonSessionManager: Session = {
        let configuration = URLSessionConfiguration.default
        configuration.httpShouldSetCookies = false
        let manager = Session(configuration: configuration)
        return manager
    }()

    private let configuration = Configuration()
    private let errorParser = ErrorParser()

    private(set) lazy var networkService: NetworkService = NetworkServiceImpl(
        errorParser: errorParser,
        sessionManager: commonSessionManager)

    private init() {}

    /// Создание сервиса по работе с авторизацией
    func makeAuthRequestFactory() -> AuthService {
        return AuthServiceImpl(baseUrl: configuration.baseUrl, networkService: networkService)
    }

    /// Создание сервиса по работе с продуктами
    func makeProductsRequestFactory() -> ProductService {
        return ProductServiceImpl(baseUrl: configuration.baseUrl, networkService: networkService)
    }

    /// Создание сервиса по работе с корзиной
    func makeBasketRequestFactory() -> BasketService {
        return BasketServiceImpl(baseUrl: configuration.baseUrl, networkService: networkService)
    }

    /// Создание сервиса по работе с отзывами
    func makeReviewRequestFactory() -> ReviewService {
        return ReviewServiceImpl(baseUrl: configuration.baseUrl, networkService: networkService)
    }
}
