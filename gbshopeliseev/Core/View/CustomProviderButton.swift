import UIKit

// Класс описывающий custom'ую кнопку при авторизацию черех Facebook и Google
class CustomProviderButton: UIButton {

    let title: String
    let image: UIImage

    init(title: String, image: UIImage) {
        self.title = title
        self.image = image
        super.init(frame: .zero)
        setTitle(title, for: .normal)
        setImage(image, for: .normal)
        setTitleColor(.white, for: .normal)
        layer.cornerRadius = 5
        translatesAutoresizingMaskIntoConstraints = false
        heightAnchor.constraint(equalToConstant: 50).isActive = true
        image.withTintColor(.white, renderingMode: .alwaysOriginal)
    }

    override func layoutSubviews() {
        super.layoutSubviews()
        if imageView != nil {
            imageEdgeInsets = UIEdgeInsets(top: 10, left: 10, bottom: 10, right: bounds.width - 45)        }
    }

    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
