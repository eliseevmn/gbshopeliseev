import UIKit

// Класс описывающий custom'ую кнопку при авторизацию черех Email
class CustomAuthButton: UIButton {

    let title: String
    let heightButton: CGFloat = 50

    init(title: String) {
        self.title = title
        super.init(frame: .zero)
        setTitle(title, for: .normal)
        backgroundColor = #colorLiteral(red: 0.1411764771, green: 0.3960784376, blue: 0.5647059083, alpha: 1)
        setTitleColor(.white, for: .normal)
        heightAnchor.constraint(equalToConstant: heightButton).isActive = true
        titleLabel?.font = UIFont.boldSystemFont(ofSize: 16)
        layer.cornerRadius = heightButton/2
        layer.masksToBounds = true
    }

    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
