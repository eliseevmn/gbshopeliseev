import UIKit

// Класс описывающий UITextField's при авторизации и регистрации
class CustomTextField: UITextField {

    let cornerRadius: CGFloat
    let height: CGFloat
    let fontSize: CGFloat
    private let defaultUnderlineColor = UIColor.black
    private let bottomLine = UIView()

    private var labelYAnchorConstraint: NSLayoutConstraint!
    private var labelLeadingAnchor: NSLayoutConstraint!

    let label: UILabel = {
        let label = UILabel()
        label.alpha = 0.6
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()

    init(cornerRadius: CGFloat, height: CGFloat, fontSize: CGFloat, labelText: String) {
        self.cornerRadius = cornerRadius
        self.height = height
        self.fontSize = fontSize
        self.label.text = labelText
        super.init(frame: .zero)
        textColor = .gray
        layer.cornerRadius = cornerRadius
        font = UIFont.boldSystemFont(ofSize: fontSize)
        configureViews()
        delegate = self
    }

    override var intrinsicContentSize: CGSize {
        return .init(width: 0, height: height)
    }

    public func setUnderlineColor(color: UIColor = .red) {
        bottomLine.backgroundColor = color
    }

    public func setDefaultUnderlineColor() {
        bottomLine.backgroundColor = defaultUnderlineColor
    }

    func configureViews() {
        borderStyle = .none
        bottomLine.backgroundColor = defaultUnderlineColor

        self.addSubview(bottomLine)
        bottomLine.anchor(top: nil, leading: leadingAnchor, bottom: bottomAnchor, trailing: trailingAnchor)
        bottomLine.heightAnchor.constraint(equalToConstant: 1).isActive = true
        bottomLine.widthAnchor.constraint(equalToConstant: frame.width).isActive = true

        self.addSubview(label)
        labelYAnchorConstraint = label.centerYAnchor.constraint(equalTo: centerYAnchor)
        labelLeadingAnchor = label.leadingAnchor.constraint(equalTo: leadingAnchor)
        NSLayoutConstraint.activate([
            labelLeadingAnchor,
            labelYAnchorConstraint
        ])
    }

    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}

extension CustomTextField: UITextFieldDelegate {

    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        return textField.resignFirstResponder()
    }

    func textFieldDidBeginEditing(_ textField: UITextField) {
        labelYAnchorConstraint.constant = -30
        labelLeadingAnchor.constant = -15
        label.textColor = .red
        label.alpha = 0.6
        performAnimation(transform: CGAffineTransform(scaleX: 0.8, y: 0.8))
    }

    func textFieldDidEndEditing(_ textField: UITextField) {
        if let text = textField.text, text.isEmpty {
            labelYAnchorConstraint.constant = 0
            labelLeadingAnchor.constant = 0
            label.textColor = .black
            label.alpha = 0.5
            performAnimation(transform: CGAffineTransform(scaleX: 1, y: 1))
        }
    }

    fileprivate func performAnimation(transform: CGAffineTransform) {
        UIView.animate(withDuration: 0.5,
                       delay: 0,
                       usingSpringWithDamping: 1,
                       initialSpringVelocity: 1,
                       options: .curveEaseOut,
                       animations: {
            self.label.transform = transform
            self.layoutIfNeeded()
        }, completion: nil)
    }
}
