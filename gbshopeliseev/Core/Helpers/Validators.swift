import Foundation

/// Класс описывающий результаты проверок
class Validators {

    static func isFillled(login: String?, password: String?) -> Bool {
        guard !(login ?? "").isEmpty,
            !(password ?? "").isEmpty else {
                return false
        }
        return true
    }

    static func isSimpleEmail(_ email: String) -> Bool {
        let emailRegEx = "^.+@.+\\..{2,}$"
        return check(text: email, regEx: emailRegEx)
    }

    public static func check(text: String, regEx: String) -> Bool {
        let predicate = NSPredicate(format: "SELF MATCHES %@", regEx)
        return predicate.evaluate(with: text)
    }

    static func notConfirmPasswords(password: String, confirmPassword: String?) -> Bool {
        guard password == confirmPassword else {
            return false
        }
        return true
    }
}
