import XCTest

class GbshopeliseevUITests: XCTestCase {

    var app: XCUIApplication!

    override func setUp() {
        super.setUp()
        continueAfterFailure = false
        app = XCUIApplication()
        setupSnapshot(app)
        app.launch()
    }

    func testSuccessLoginInWithEmail() {
        snapshot("LoginScreen")
        enterAuthData(login: "111@gmail.com", password: "111111")
    }

    func testFailLoginInWithEmail() {
        enterAuthData(login: "111@gmail.com", password: "")
    }

    func testSuccesToRegistrationScreen() {
        let button = app.buttons["Не зарегистрированы?"]
        button.tap()
        snapshot("Registration Screen")
    }

    private func enterAuthData(login: String, password: String) {
        let emailTextField = app.textFields["emailTextField"]
        emailTextField.tap()
        emailTextField.typeText(login)

        let passwordTextField = app.secureTextFields["passwordTextField"]
        passwordTextField.tap()
        passwordTextField.typeText(password)

        let button = app.buttons["ВОЙТИ"]
        button.tap()
        XCTAssertTrue(button.exists)
    }
}
